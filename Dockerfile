FROM        python:3.9.5
MAINTAINER  Jelle Smet
COPY        setup.py /var/tmp
COPY        setup.cfg /var/tmp
ADD         hello_planet /var/tmp/hello_planet
WORKDIR     /var/tmp
RUN         python setup.py install
ENTRYPOINT  ["hello-planet", "--address", "0.0.0.0"]
