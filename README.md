# hello-csms-ws

A simple webservice to test container based architectures. 

## Usage

```bash
hello-csms-ws --port 100080 --address 0.0.0.0
```
