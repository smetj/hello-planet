#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  __init__.py
#
#  Copyright 2021 Jelle Smet <development@smetj.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from gevent import monkey

monkey.patch_all()
from gevent import pywsgi
from gevent import socket
from gevent.pool import Pool
from gevent import sleep
import falcon
import argparse


class Greeting(object):
    def __init__(self):
        self.hostname = socket.gethostname()

    def on_get(self, req, resp):

        resp.text = f"Greetings from '{self.hostname}' earthling.\n"


class FalconServer(object):
    def __init__(
        self,
        address="127.0.0.1",
        port=80,
        ssl_key=None,
        ssl_cert=None,
        ssl_cacerts=None,
        poolsize=100,
        so_reuseport=False,
    ):

        self.address = address
        self.port = port
        self.ssl_key = ssl_key
        self.ssl_cert = ssl_cert
        self.ssl_cacerts = ssl_cacerts
        self.poolsize = poolsize
        self.so_reuseport = so_reuseport

        self.app = falcon.App()
        self.app.add_route("/", Greeting())

        if self.ssl_key is not None and self.ssl_cert is not None:
            ssl_options = {
                "keyfile": self.ssl_key,
                "certfile": self.ssl_cert,
                "ca_certs": self.ssl_cacerts,
            }
        else:
            ssl_options = {}

        self.server = pywsgi.WSGIServer(
            listener=self.__getListener(),
            application=self.app,
            spawn=Pool(self.poolsize),
            **ssl_options,
        )

    def start(self):
        print(f"Serving requests on {self.address}:{self.port}")
        self.server.start()

    def stop(self):
        self.server.stop()

    def __getListener(self):

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        if self.so_reuseport:
            sock.setsockopt(socket.SOL_SOCKET, 15, 1)

        sock.setblocking(0)
        sock.bind((self.address, self.port))
        sock.listen(50)
        return sock


def parse_arguments():

    parser = argparse.ArgumentParser(
        description="The universal hello world web service"
    )
    parser.add_argument(
        "--port",
        type=int,
        dest="port",
        default="80",
        help="The webservice port to bind to.",
    )
    parser.add_argument(
        "--address",
        type=str,
        dest="address",
        default="127.0.0.1",
        help="The webservice interface to bind to.",
    )

    return parser.parse_args()


def main():

    args = parse_arguments()

    f = FalconServer(address=args.address, port=args.port)
    f.start()
    try:
        while True:
            sleep(1)
    except KeyboardInterrupt as _:
        print("Exit.")
    else:
        raise


if __name__ == "__main__":
    main()
